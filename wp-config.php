<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define('DB_NAME', 'kanditre_kanditr');

/** Username của database */
define('DB_USER', 'kanditre_kanditr');

/** Mật khẩu của database */
define('DB_PASSWORD', 'pPD9O02w');

/** Hostname của database */
define('DB_HOST', 'localhost');

/** Database charset sử dụng để tạo bảng database. */
define('DB_CHARSET', 'utf8mb4');

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~V]}?Er0$`=4fDR*@vd(R-aBqp(:!j&,M6:s_#=gEkg[;YT0Gdh3h08d2Y>XQZR~');
define('SECURE_AUTH_KEY',  '7e5U7O)_,|Nq;h<y~Gx4rx]Fv_S6u+08IGz7?3hF3]H`2v02>i( X]dbek$K#y6C');
define('LOGGED_IN_KEY',    'u:[4gFBP)g%p_}HMEi:!3[v3L-#`Gmtq5,^j{)V8DP[L}wTaz/9w6e/9O{,U1iXa');
define('NONCE_KEY',        'qM!{SH7b<_xsn <,uc.fF5<*psyppv>`3YwCyyupI`JLIFOdd_,.dXoiXEu-~Z/k');
define('AUTH_SALT',        'l|X`,~YF&&V(_G5Tpz|U]#qfZ:y{j~}NDA%g_8mr|8=lGAZO-x_iT:<bT2~#sn8:');
define('SECURE_AUTH_SALT', '~<v ,lyowj~TYu1WE9zo:.@v:&oRDib@|;lv!1`<uyKpW(XG}zX*fr2Z/74V7jwk');
define('LOGGED_IN_SALT',   'XJ)z8<AN-X{P!!!+,]nk_hV-Ag|jn6diamTC}k!e9oMz(ha[6QQ_[~^AaF.D&Fep');
define('NONCE_SALT',       '%~,bsaS3Vu7ESQJz-G<qC =zJhavM|P7-_Q7+=?^8}u1a6d3X{#8FFKWg};vT`^E');

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'trungams_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
