<?php
get_header( );

while ( have_posts() ) : the_post();
    global $post;
    ?>
    <div class="per-videok">
        <div class="video-content">
            <?php /*$url = get_post_meta( get_the_ID(), 'web_url', true );
			echo html_entity_decode($url);*/
            ?>
            <video  id="hero-block__video" autoplay loop muted playsinline><source src="<?php echo $url = get_post_meta( get_the_ID(), 'web_url', true ); ?>" type="video/mp4"></video>
        </div>
        <div class="per-titlek"><h2 class='tlp-member-title'><?php the_title(); ?></h2></div>
    </div>
    <div class="tlp-team tlp-single-container">

        <article id="post-<?php the_ID(); ?>" <?php post_class('tlp-member-article'); ?>>
            <div class="row">
                <div class="col-lg-5 col-md-6 col-xs-12 tlp-member-feature-img">
                    <div class="img-perk">
                        <a href="<?php echo get_site_url(); ?>/contact">
                            <?php
                            if(has_post_thumbnail()){
                                echo get_the_post_thumbnail( get_the_ID(), 'large' );
                            }
                            ?>
                        </a>
                    </div>


                </div>
                <div class="col-lg-7 col-md-6 col-xs-12">

                    <div class="tlp-member-detail"><?php the_content(); ?></div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="per-social">
                        <?php  $name=get_the_title();
                        $firstname = explode(' ',trim($name));

                        ?>
                        <a class="ask-personk" href="<?php echo get_site_url(); ?>/contact">Ask about <?php echo $firstname[0]; ?></a>
                        <a class="links-personk"><?php echo $designation = get_post_meta( get_the_ID(), 'designation', true ); ?></a>
                        <a class="links-personk" href="<?php echo get_post_meta( get_the_ID(), 'location', true ); ?>" target="_blank">
                            <?php echo $instaAdress = get_post_meta( get_the_ID(), 'email', true ); ?>
                        </a>
                        <a class="links-personk" href="<?php echo get_site_url(); ?>/talent-pr"><?php _e('Back to search'); ?></a>
                        <?php
                        $s = unserialize(get_post_meta( get_the_ID(), 'social' , true));
                        $html = null;
                        $html .= '<div class="tpl-social">'; ?>
                        <span class="share-detail-cele"><?php _e('Share this speaker’s details'); ?></span>
                             <?php
                        if($s){
                            foreach ($s as $id => $link) {
                                $html .= "<a class='fa fa-$id' href='{$s[$id]}' title='$id' target='_blank'><i class='fa fa-$id'></i></a>";
                            }
                        }
                        $html .= '<br></div>';
                        echo $html;
                        ?>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <?php echo do_shortcode('[shortcode_modal_talent_demo]'); ?>
                </div>
            </div>
        </article>

    </div>

<?php endwhile;
get_footer();
