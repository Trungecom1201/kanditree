<?php
/*
 Template Name: Blog
 */
get_header();
?>
<div class="blog-page">
<div class="event-hero-block block">
    <div class="event-hero-block__wrapper">
        <div class="event-hero-block__video event-inline-background-cover">
            <video id="event-hero-block__video" muted="true" autoplay="true" loop="true">
                <source type="video/mp4" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/videoplayback.mp4-00.01.19.000-00.02.10.375.mp4<?php the_field('video_homepage'); ?>">
            </video>
        </div>
        <div class="event-hero-block__contents">
            <h1 class="event-hero-block__title">The talent & communications agency<?php the_field('title_video_block'); ?></h1>
        </div>
    </div>
</div>
</div>
    <section class="module module-talent">
        <div class="container">
            <div class="row">

                <!-- Content column start -->
                <div class="col-sm-12">
                    <?php
                    /**
                     * Top of the content hook.
                     *
                     * @hooked woocommerce_breadcrumb - 10
                     */
                    do_action( 'shop_isle_content_top' );

                    while ( have_posts() ) {
                        the_post();

                        do_action( 'shop_isle_page_before' );

                        get_template_part( 'content', 'page' );

                        /**
                         * Bottom of content hook.
                         *
                         * @hooked shop_isle_display_comments - 10
                         */
                        do_action( 'shop_isle_page_after' );
                    }
                    ?>
                </div>
            </div> <!-- .row -->

        </div>
    </section>
<?php get_footer(); ?>