<?php
/*
 Template Name: Event
 */
get_header();
?>
    <div class="event-hero-block block">
        <div class="event-hero-block__wrapper">
            <div class="event-hero-block__video event-inline-background-cover">
                <video id="event-hero-block__video" muted="true" autoplay="true" loop="true">
                    <source type="video/mp4" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/videoplayback.mp4-00.01.19.000-00.02.10.375.mp4<?php the_field('video_homepage'); ?>">
                </video>
            </div>
            <div class="event-hero-block__contents">
                <h1 class="event-hero-block__title">The talent & communications agency<?php the_field('title_video_block'); ?></h1>
            </div>
        </div>
    </div>
    <div class="sm-text-for-SEO"><p> Kandi tree work with lots of celebrities and influencers that can deliver high traffic and followers to your personal and corporate Instagram accounts. We offer services such as Content creation for your brand, Brand collaboration and endorsement, Increased followers with relevance to your brand and full social media account control.  Below are the celebrities and influencers we work with that could push your visibility and brand into new customers and deliver improved sales and brand value.</p></div>
        <div class="col-md-12 sm-title">Dawn Ward</div>
        <div class="col-md-4 col-sm-6" >
            <img class="page-sm-img" src='http://localhost/kanditree/wp-content/uploads/2018/11/img2.png'></div>
        <div class="col-md-4 col-sm-6">
            <img class="page-sm-img" src='http://localhost/kanditree/wp-content/uploads/2018/11/img1.png'></div>
        <div class="col-md-4 col-sm-6">
            <img class="page-sm-img" src='http://localhost/kanditree/wp-content/uploads/2018/11/img1.png'></div>
        <div class="col-md-12 col-sm-12 SEO-text">
            <p>Dawn has over 322k Followers and impressions reach up to 7,000,000 depending on content and task. Dawn has worked with brands such as Jet2, Daniel Footwear and Swinton insurance. Most Famous for her starring role in ITVbe’s Real Housewives of Cheshire, dawn has also appeared regularly on prime time TV and has a diverse following. </p>
        </div><a class="sm-but" href="#">Enquire</a>




<?php get_footer(); ?>