<?php
/*
 Template Name: gallery
 */

get_header(); ?>

<!-- Wrapper start -->
<div class="main">
    <!-- Header section start -->
    <div class="event-hero-block block">
        <div class="event-hero-block__wrapper">
            <div class="event-hero-block__video event-inline-background-cover">
                <video id="event-hero-block__video" muted="true" autoplay="true" loop="true">
                    <source type="video/mp4" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/videoplayback.mp4-00.01.19.000-00.02.10.375.mp4<?php the_field('video_homepage'); ?>">
                </video>
            </div>
            <div class="event-hero-block__contents">
                <h1 class="event-hero-block__title">The talent & communications agency<?php the_field('title_video_block'); ?></h1>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <!--<h1 class="module-title font-alt"><?php /*the_title(); */?></h1>-->
                <?php
                /* Header description */

                $shop_isle_shop_id = get_the_ID();

                if ( ! empty( $shop_isle_shop_id ) ) :

                    $shop_isle_page_description = get_post_meta( $shop_isle_shop_id, 'shop_isle_page_description' );

                    if ( ! empty( $shop_isle_page_description[0] ) ) :
                        echo '<div class="module-subtitle font-serif mb-0">' . wp_kses_post( $shop_isle_page_description[0] ) . '</div>';
                    endif;

                endif;
                ?>
            </div>

        </div><!-- .row -->

    </div>
</div>
    </section>
    <!-- Header section end -->

    <!-- Pricing start -->
    <section class="module module-gallery">
        <div class="container">
            <div class="row">

                <!-- Content column start -->
                <div class="col-sm-12">
                    <?php
                    /**
                     * Top of the content hook.
                     *
                     * @hooked woocommerce_breadcrumb - 10
                     */
                    do_action( 'shop_isle_content_top' );

                    while ( have_posts() ) {
                        the_post();

                        do_action( 'shop_isle_page_before' );

                        get_template_part( 'content', 'page' );

                        /**
                         * Bottom of content hook.
                         *
                         * @hooked shop_isle_display_comments - 10
                         */
                        do_action( 'shop_isle_page_after' );
                    }
                    ?>
                </div>
            </div> <!-- .row -->

        </div>
    </section>
    <!-- Pricing end -->

</div>
    <?php get_footer(); ?>
