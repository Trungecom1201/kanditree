<?php
/*
 Template Name: Talent
 */
get_header();
?>

        <?php echo get_post_field('post_content'); ?>

<div class="hero-block block">
    <div class="hero-block__wrapper">
        <div class="hero-block__video inline-background-cover">
            <video id="hero-block__video" muted="true" autoplay="true" loop="true">
                <source type="video/mp4" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/videoplayback.mp4-00.01.19.000-00.02.10.375.mp4">
            </video>
        </div>
        <div class="hero-block__contents">
            <h1 class="hero-block__title"><?php the_field('title_video_block'); ?></h1>
        </div>
    </div>
</div>

<div class="main-content-talent">

    <div class="form-search-talent">
        <form class="form-search_form_details">
            <div class="search_form_details">
                <input type="text" class="input-search-talent" placeholder="SEARCH TALENT">
                <button type="submit" class="submit-search-talent"><i class="fa fa-long-arrow-down"></i></button>
            </div>
        </form>
    </div>
    <div class="show-search-talent">
        <div class="influencers_talent">
            <div class="heading-influencers">
                <h3 class="heading-influencers_talent">INFLUENCERS</h3>
            </div>
            <div class="show_influencers_talent">

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/Untitledqqqd-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/Untitlcaaaed-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/Undtitled-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/dqw1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/dq-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/aa1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="personal_talent">
            <div class="heading-influencers">
                <h3 class="heading-influencers_talent">PERSONAL APPEARANCES</h3>
            </div>
            <div class="show_influencers_talent">

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/Untitledqqqd-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/Untitlcaaaed-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/Undtitled-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/dqw1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/dq-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/aa1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/dq-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/aa1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details information-talent">
                    <h4 class="title-can-find">Can't find what</h4>
                    <h4 class="title-you-looking">you're looking for?</h4>
                    <span class="title-call-talent">Call: 01275 463222</span>
                    <span class="title-email-talent">Email: agents@gordonpoole.com</span>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/dq-1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="influencers_talent_details">
                    <div class="img-talent_details">
                        <a href="#"><img class="img-details-talent" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/aa1.jpg"/></a>
                    </div>
                    <div class="content_influencers_talent_details">
                        <h4 class="title-heading-talent">adam tufnell</h4>
                        <p>round-the-world sailor</p>
                        <a href="#">biography <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
