<?php
/**
 * Template functions used for the site header.
 *
 * @package shop-isle
 */

if (!function_exists('shop_isle_primary_navigation')) {
    /**
     * Display Primary Navigation
     *
     * @since  1.0.0
     * @return void
     */
    function shop_isle_primary_navigation()
    {

        $navbar_class = '';
        $hide_top_bar = get_theme_mod('shop_isle_top_bar_hide', true);
        if ((bool)$hide_top_bar === false) {
            $navbar_class .= ' header-with-topbar ';
        }

        $shop_isle_blog_case = 0;
        $shop_isle_front_page_case = 0;
        $shop_isle_default_template_case = 0;

        $shop_isle_wporg_flag = get_option('shop_isle_wporg_flag');

        if (!empty($shop_isle_wporg_flag) && ('true' === $shop_isle_wporg_flag)) {

            if ('page' == get_option('show_on_front')) {
                $shop_isle_keep_old_fp_template = get_theme_mod('shop_isle_keep_old_fp_template');
                if (!$shop_isle_keep_old_fp_template && is_front_page()) {
                    $shop_isle_front_page_case = 1;
                }
            }
        } else {
            if ('posts' == get_option('show_on_front') && is_front_page()) {
                $shop_isle_front_page_case = 1;
            }
        }

        if ($shop_isle_front_page_case) {
            $navbar_class .= ' navbar-color-on-scroll navbar-transparent ';
        }

        ?>
        <!-- Navigation start -->

        <nav class="navbar navbar-custom navbar-fixed-top <?php echo esc_attr($navbar_class); ?>" role="navigation">

            <div class="header-container">
                <div class="navbar-header">
                    <div class="header-block__top-wrap">
                        <div class="header-block__logos">
                            <?php echo '<a href="' . esc_url(home_url('/')) . '" class="header-block__logos--wbstl-wrapper" tabindex="0"> '?>
                            <img class="header-block__logos--wbstl" src="/wp-content/uploads/2018/12/KT-FULL-WhiteonBlack-RGB-300-3.png" alt="Logo">
                            </a>
                        </div>

                        <div class="header-block__menu-wrap">
                            <div class="header-block__menu-wrap-inner">
                                <div id="header-block__toggle" >
                                    <p>Menu</p>
                                </div>
                            </div>
                        </div>

                        <script>
                            jQuery(document).ready(function ($) {
                                $("#header-block__toggle").click(function() {
                                    $(this).toggleClass('changebg');
                                });
                            });
                        </script>
                    </div>

                    <div class="header-block__nav-wrap">
                        <div class="header-block__nav-wrap-inner">
                            <ul class="header-block__nav-wrap-inner--main">
                                <li class="header-block__nav-header">

                                    <strong>
                                        <a href="/events">Events</a>
                                    </strong>
                                </li>

                            </ul>
                            <ul class="header-block__nav-wrap-inner--main">
                                <li class="header-block__nav-header">
                                    <strong>
                                        <a href="/talent-pr">Talent</a>
                                    </strong>
                                </li>

                            </ul>

                            <ul class="header-block__nav-wrap-inner--main">
                                <li class="header-block__nav-header">
                                    <strong>
                                        <a href="/pr">PR</a>
                                    </strong>
                                </li>
                            </ul>
                            <ul class="header-block__nav-wrap-inner--main">
                                <li class="header-block__nav-header">
                                    <strong>
                                        <a href="/social-media">Social Media</a>
                                    </strong>
                                </li>
                            </ul>
                            <ul class="header-block__nav-wrap-inner--main">
                                <li class="header-block__nav-header">
                                    <strong>
                                        <a href="/gallery">Gallery</a>
                                    </strong>
                                </li>
                            </ul>
                            <ul class="header-block__nav-wrap-inner--main">
                                <li class="header-block__nav-header">
                                    <strong>
                                        <a href="/blog">Blog</a>
                                    </strong>
                                </li>
                            </ul>
                            <ul class="header-block__nav-wrap-inner--main">
                                <li class="header-block__nav-header">
                                    <strong>
                                        <a href="/contact">Contact us</a>
                                    </strong>
                                </li>
                            </ul>
                            <ul class="header-block__nav-wrap-inner--main infor-menu">
                                <li class="header-block__nav-header">
                                    <a class="menu-tel" href="tel:01565 745668">01565 745668</a><br>
                                    <a class="menu-mail" href="mailto:enquiries@kanditree.co.uk">enquiries@kanditree.co.uk</a>
                                    <!-- 									<a class="menu-tel" href="tel:<?php the_field('phone_header'); ?>"><?php the_field('phone_header'); ?></a><br>
                                    <a class="menu-mail" href="mailto:<?php the_field('email_header'); ?>"><?php the_field('email_header'); ?></a> -->
                                </li>
                                <form role="search" method="get" class="search-form header" id="searchform" action="<?php get_site_url(); ?>/talent-pr/">
                                    <label>
                                        <span class="screen-reader-text">Search for:</span>
                                        <input type="text" name="search" id="search" class="search-field" placeholder="Search talent…">
                                    </label>
                                    <input type="submit" class="search-submit" value="Search" />
                                </form>
                            </ul>

                        </div>


                        <div class="header-block__secondary-wrap">
                            <!--                            <div class="header-block__additional-links">-->
                            <!--                                <button id="header-block__language-toggle" class="header-block__language-toggle">-->
                            <!--                                    <span>Language</span>English-->
                            <!--                                </button>-->
                            <!--                                <div class="header-block__additional-links__search">-->
                            <!--                                    <button id="header-block__search-toggle" class="header-block__search-toggle" style="background: no-repeat url(-->
                            <!--                                        </button>-->
                            <!--                                    </div>-->
                            <!--                                </div>-->

                            <div class="header-block__search__overlay">
                                <div class="header-block__search__close">
                                    <p>Close</p>
                                    <button class="header-block__search__close-button">
                                        <span class="vhdn">Close Search</span>

                                    </button>
                                </div>

                                <div class="header-block__search__input-wrapper">
                                    <form id="page-search__form-search" role="search" method="get" action="https://www.wbstudiotour.co.uk/faq-search" accept-charset="utf-8">
                                        <label for="page-search__search" class="vhdn">Search WBSTL</label>
                                        <input id="header-block__search__input" class="header-block__search__input" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" name="keys" placeholder="Search FAQs" value="">
                                        <div class="header-block__search__input-btn-container">
                                            <button type="submit" name="search-button" class="header-block__search__input-btn header-block-search__input-field" id="header-block__search__input-btn"><span class="vhdn">Search WBSTL</span></button>
                                        </div>
                                    </form>
                                </div>

                                <div class="header-block__search__popular-searches">
                                    <h4>Popular Searches</h4>
                                    <ul>
                                        <li>
                                            <a href="https://www.wbstudiotour.co.uk/faq-search?keys=Ticket+Prices" class="header-block__search__popular-searches-search">
                                                Ticket Prices                        </a>
                                        </li>
                                        <li>
                                            <a href="https://www.wbstudiotour.co.uk/faq-search?keys=Opening+Times" class="header-block__search__popular-searches-search">
                                                Opening Times                        </a>
                                        </li>
                                        <li>
                                            <a href="https://www.wbstudiotour.co.uk/faq-search?keys=Ticket+Availability" class="header-block__search__popular-searches-search">
                                                Ticket Availability                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="header-block__language-menu">
                                <div class="header-block__language-menu__top">
                                    <p>Choose a Language</p>
                                    <button id="header-block__language-menu__close" class="header-block__language-menu__close">
                                        <span>Close</span>

                                    </button>
                                </div>
                                <ul>
                                    <li class="selected">
                                        <a href="/">English</a>
                                    </li>
                                    <li>
                                        <a href="/fr">French</a>
                                    </li>
                                    <li>
                                        <a href="/de">German</a>
                                    </li>
                                    <li>
                                        <a href="/es">Spanish</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (class_exists('WooCommerce')) : ?>
                    <div class="navbar-cart">

                        <div class="header-search">
                            <div class="glyphicon glyphicon-search header-search-button"></div>
                            <div class="header-search-input">
                                <form role="search" method="get" class="woocommerce-product-search"
                                      action="<?php echo esc_url(home_url('/')); ?>">
                                    <input type="search" class="search-field"
                                           placeholder="<?php echo esc_attr_x('Search Products&hellip;', 'placeholder', 'shop-isle'); ?>"
                                           value="<?php echo get_search_query(); ?>" name="s"
                                           title="<?php echo esc_attr_x('Search for:', 'label', 'shop-isle'); ?>"/>
                                    <input type="submit"
                                           value="<?php echo esc_attr_x('Search', 'submit button', 'shop-isle'); ?>"/>
                                    <input type="hidden" name="post_type" value="product"/>
                                </form>
                            </div>
                        </div>

                        <?php if (function_exists('WC')) : ?>
                            <div class="navbar-cart-inner">
                                <a href="<?php echo esc_url(wc_get_cart_url()); ?>"
                                   title="<?php esc_attr_e('View your shopping cart', 'shop-isle'); ?>"
                                   class="cart-contents">
                                    <span class="icon-basket"></span>
                                    <span class="cart-item-number"><?php echo esc_html(trim(WC()->cart->get_cart_contents_count())); ?></span>
                                </a>
                                <?php apply_filters('shop_isle_cart_icon', ''); ?>
                            </div>
                        <?php endif; ?>

                    </div>
                <?php endif; ?>

            </div>

        </nav>
        <!-- Navigation end -->
        <?php
    }
}// End if().
