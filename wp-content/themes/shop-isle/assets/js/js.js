jQuery(document).ready(function ($) {
    // Handler for .ready() called.
    $("#header-block__toggle").click(function(){
        $(".header-block__nav-wrap").slideToggle(100);
    });

    // $(window).scroll(function(){
    //     var scroll = $(window).scrollTop();
    //     if (scroll > 170) {
    //         $('.navbar-fixed-top').addClass('changebgnav');
    //     }
    //     else {
    //         $('.navbar-fixed-top.changebgnav').removeClass('changebgnav');
    //     }
    // })

    var welmnt = $(".calendar-block__event--details").width();
    var helmnt = 2/3*welmnt;
    var helmntpx = helmnt+"px";

    $(".calendar-block__event--details").height(helmntpx);

    $(".layout-2 .team-member:nth-child(7)").after('<div class="influencers_talent_details col-md-6 col-sm-6 col-xs-12"><div class="total-information"><h4 class="title-can-find">Can\'t find what</h4><h4 class="title-you-looking">you\'re looking for?</h4><div class="infor-talent-details"><span class="title-call-talent">Call: 01275 463222</span><span class="title-email-talent">Email: agents@gordonpoole.com</span></div></div></div></div>');

    $(".content-talent-s .search-form .icon-magnifying-glass").click(function(){
        $(".content-talent-s .search-form .search-submit").trigger("click");
    });

    $('.wpb_wrapper .products .product .woocommerce-LoopProduct-link .price').append('<span class="book-ticket-product">Book ticket</span>')

    $(".product-main-content .woocommerce-breadcrumb").append('<div class="breadcrumb-product"></div>');
    $(".product-main-content .woocommerce-breadcrumb").append('<span class="shop-product">Shop</span>');
    $(".product-main-content .woocommerce-breadcrumb .a-bread-pro").appendTo(".product-main-content .woocommerce-breadcrumb .breadcrumb-product");
    $(".product-main-content .woocommerce-breadcrumb .shop-product").appendTo(".product-main-content .woocommerce-breadcrumb .breadcrumb-product");
    $(".product-main-content .woocommerce-breadcrumb .span-bread-pro").appendTo(".product-main-content .woocommerce-breadcrumb .breadcrumb-product");
    $("#post-49 .contact-link.button").appendTo("#post-49 .row-rl-gallery-page");
    $(".product-main-content .woocommerce-breadcrumb .breadcrumb-product .a-bread-pro:nth-child(2)").click(function(){
        $(".product-main-content .woocommerce-breadcrumb .breadcrumb-product .a-bread-pro:nth-child(2)").href="http://kanditree.wsoftpro.com/tickets";

    });

    $(".header-container .navbar-cart").appendTo(".header-container .header-block__nav-wrap .infor-menu .header-block__nav-header");
//  $(".navbar-cart .navbar-cart-inner .cart-contents").append('<span class="cart-icon-besket">BASKET <i class="fa fa-chevron-right"></i></span>');
    $("#post-555 .event-hero-block__title").text("tickets and auctions");
    $(".post-header.font-alt .post-title.entry-title ").append('<span class="back-event"><i class="fa fa-long-arrow-left"></i>Back</span>');
    $(".post-entry.entry-content .rl-gallery-container ").append('<span class="back-event"><i class="fa fa-long-arrow-left"></i>Back</span>');
    $(".back-event").click(function(){
        window.history.back();
    });
    $(".single-post-lightbox .perpostlb-has-video .bg-per-post-lightbox").append('<i class="fa fa-play-circle"></i>');

    $(".per-social a:nth-child(4)").click(function(){
        window.history.back();
        return false;
    });

    var h1 = $(".per-social").height();
    var h2 = 15;
    var tt = h1 - h2;

    $(".tlp-team.tlp-single-container .portfolio-slides .single a p").css("height", tt);

    $(".rl-gallery-item .rl-gallery-item-content").append('<div class="rl-gallery-search"><img src="/wp-content/uploads/2018/12/698956-icon-111-search-512.png"></div>');
    $(".wplightbox .bg-per-post-lightbox").append('<div class="rl-gallery-search"><img src="/wp-content/uploads/2018/12/698956-icon-111-search-512.png"></div>');

});