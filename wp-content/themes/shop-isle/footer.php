<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Shop Isle
 */
?>
<div class="footer-block__pages-links-wrapper">
    <div class="footer-block__menu-studio footer-block__menu">
        <ul class="footer-block__menu-studio--london">
            <h3 class="head-ft">Kandi tree</h3>

            <li>
                <a href="/about-us">
                    About Us
                </a>
            </li>
            <li>
                <a href="/modern-slavery-statement">
                    Modern slavery statement
                </a>
            </li>
            <li>
                <a href="#">Privacy Statement</a>
            </li>
            </li>
        </ul>

    </div>

    <div class="footer-block__menu-xsell footer-block__menu">
        <div class="footer-block__menu--wrapper">
            <h3 class="head-ft">SEVICES</h3>
            <ul>
                <li>
                    <a href="http://kanditree.wsoftpro.com/gallery/" >Gallery</a>
                </li>
                <li>
                    <a href="http://kanditree.wsoftpro.com/contact/" >Contact us</a>
                </li>
                <li>
                    <a href="http://kanditree.wsoftpro.com/events/" >Events</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="footer-block__menu-experience footer-block__menu">
        <div class="footer-block__menu--wrapper">
            <h3 class="head-ft">Email Us</h3>
            <ul>
                <li>
                    <a href="#" >
                        enquiries@kanditree.co.uk
                    </a>
                </li>
                <li>
                    <a href="#">
                        Warford Hall
                    </a>
                </li>
                <li>
                    <a href="#">
                        Alderley edge
                    </a>
                </li>
                <li>
                    <a href="#">
                        CHESHIRE SK9 7TE
                    </a>
                </li>
            </ul>
            <ul class="mxh">
                <li>
                    <a id="instagram" href="https://www.instagram.com/teamkanditree/">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/shop-isle/assets/images/ista-icon.png">
                    </a>
                </li>
                <li>
                    <a id="facebook" href="#">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/shop-isle/assets/images/icon-fb.png">
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <?php do_action( 'shop_isle_before_footer' ); ?>

    <?php do_action( 'shop_isle_footer' ); ?>

</div>
<!-- Wrapper end -->
<!-- Scroll-up -->
<div class="scroll-up">
    <a href="#totop"><i class="arrow_carrot-2up"></i></a>
</div>

<?php do_action( 'shop_isle_after_footer' ); ?>

<?php wp_footer(); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.min.js.map"></script>
</body>
</html>
