<?php
/**
 * The front-page.php
 *
 * @package ShopIsle
 */

get_header();
/* Wrapper start */
//
//echo '<div class="main">';
//$big_title = get_template_directory() . '/inc/sections/shop_isle_big_title_section.php';
//load_template( apply_filters( 'shop-isle-subheader', $big_title ) );
//
///* Wrapper start */
//$shop_isle_bg = get_theme_mod( 'background_color' );
//
//if ( isset( $shop_isle_bg ) && $shop_isle_bg != '' ) {
//	echo '<div class="main front-page-main" style="background-color: #' . $shop_isle_bg . '">';
//} else {
//
//	echo '<div class="main front-page-main" style="background-color: #FFF">';
//
//}
//
//if ( defined( 'WCCM_VERISON' ) ) {
//
//	/* Woocommerce compare list plugin */
//	echo '<section class="module-small wccm-frontpage-compare-list">';
//	echo '<div class="container">';
//	do_action( 'shop_isle_wccm_compare_list' );
//	echo '</div>';
//	echo '</section>';
//
//}
//
///******  Banners Section */
//$banners_section = get_template_directory() . '/inc/sections/shop_isle_banners_section.php';
//require_once( $banners_section );
//
///******* Products Section */
//$latest_products = get_template_directory() . '/inc/sections/shop_isle_products_section.php';
//require_once( $latest_products );
//
///******* Video Section */
//$video = get_template_directory() . '/inc/sections/shop_isle_video_section.php';
//require_once( $video );
//
///******* Products Slider Section */
//$products_slider = get_template_directory() . '/inc/sections/shop_isle_products_slider_section.php';
//require_once( $products_slider ); ?>

<div class="hero-block block">
    <div class="hero-block__wrapper">
        <img class="img-banner-home" src="https://wsoftpro.com/pub/media/wysiwyg/banner01.jpg" alt="banner home" width="100%">
        <div class="hero-block__video inline-background-cover">
            <video  id="hero-block__video" autoplay loop muted playsinline><source src="<?php the_field('video_homepage'); ?>" type="video/mp4"></video>
        </div>
        <div class="hero-block__contents">
            <h1 class="hero-block__title"><?php the_field('title_video_block'); ?></h1>
        </div>
    </div>
</div>

<div class="usp-block block">
    <div class="usp-block__wrapper">
        <div class="usp-block__item" style="background-image: url('<?php echo get_the_post_thumbnail_url(49); ?>');">
            <div class="usp-block__details">
                <a href="<?php echo esc_url( get_page_link( 49 ) ); ?>" class="usp-block__title-link">
                    <h3 class="usp-block__title"><?php echo get_the_title(49) ?></h3>
                </a>
                <p class="usp-block__subtitle"></p>
                <a class="usp-block__cta btn" href="<?php echo esc_url( get_page_link( 49 ) ); ?>">FIND OUT MORE</a>
            </div>
        </div>

        <div class="usp-block__item" style="background-image: url('<?php echo get_the_post_thumbnail_url(60); ?>');">
            <div class="usp-block__details">
                <a href="<?php echo esc_url( get_page_link( 60 ) ); ?>" class="usp-block__title-link">
                    <h3 class="usp-block__title"><?php echo get_the_title(60) ?></h3>
                </a>
                <p class="usp-block__subtitle"></p>
                <a class="usp-block__cta btn" href="<?php echo esc_url( get_page_link( 60 ) ); ?>">FIND OUT MORE</a>
            </div>
        </div>

        <div class="usp-block__item" style="background-image: url('<?php echo get_the_post_thumbnail_url(53); ?>');">
            <div class="usp-block__details">
                <a href="<?php echo esc_url( get_page_link( 53 ) ); ?>" class="usp-block__title-link">
                    <h3 class="usp-block__title"><?php echo get_the_title(53) ?></h3>
                </a>
                <p class="usp-block__subtitle"></p>
                <a class="usp-block__cta btn" href="<?php echo esc_url( get_page_link( 53 ) ); ?>">FIND OUT MORE</a>
            </div>
        </div>
    </div>
</div>

<div class="badges-block block">
    <h2 class="badges-block__title title title__flourish title__flourish--3">
        <img src="<?php echo get_home_url(); ?>/wp-content/themes/shop-isle/assets/images/leftarr.svg"/>
        Brands we work with
        <img src="<?php echo get_home_url(); ?>/wp-content/themes/shop-isle/assets/images/rightarr.svg"/>
    </h2>
    <?php echo do_shortcode('[wonderplugin_carousel id="1"]'); ?>
    <hr>
</div>

<div>
    <div class="calendar-block block">

        <div class="calendar-block__background" style="background-image: #fff repeat url(<?php echo get_home_url(); ?>/wp-content/themes/shop-isle/assets/images/paper-texture.jpg);"></div>

        <h2 class="calendar-block__title title title__flourish title__flourish--2">
            <svg xmlns="http://www.w3.org/2000/svg" width="70" height="24" viewBox="0 0 70 24"><path d="M20.018 4.918h3.672V4h-3.672v.918zm-5.123 7.44l-6.8-6.797-6.797 6.8 6.798 6.798 6.8-6.8zm55.105 0a.46.46 0 0 1-.46.46H15.735l-7.638 7.638L0 12.36l8.096-8.1 7.638 7.638H69.54a.46.46 0 0 1 .46.46zM27.332 4.92h10.922V4H27.332v.918zm37.62 0h3.67V4h-3.67v.918zm-23.057 0h4.853V4h-4.853v.918zm8.494 0h10.92V4H50.39v.918z" fill-rule="evenodd"/></svg>
            WHAT'S ON
            <svg xmlns="http://www.w3.org/2000/svg" width="70" height="24" viewBox="0 0 70 24"><path d="M49.982 4.918H46.31V4h3.672v.918zm5.123 7.44l6.8-6.797 6.797 6.8-6.798 6.798-6.8-6.8zM0 12.36c0 .254.206.46.46.46h53.806l7.638 7.638L70 12.36l-8.096-8.1-7.638 7.64H.46a.46.46 0 0 0-.46.46zm42.668-7.44H31.746V4h10.922v.918zm-37.62 0H1.38V4h3.67v.918zm23.057 0h-4.853V4h4.853v.918zm-8.494 0H8.69V4h10.92v.918z" fill-rule="evenodd"/></svg>
        </h2>

        <div class="calendar-block__events">
            <div class="calendar-block__event now-on" style="background-image: url('<?php echo get_the_post_thumbnail_url(46); ?>');">
                <div class="calendar-block__event--details">
                    <a href="/talent-pr"><h3 class="calendar-block__event--title"><?php echo get_the_title(46); ?></h3></a>
                    <a class="btn" href="/talent-pr">FIND OUT MORE</a>
                </div>
            </div>
            <div class="calendar-block__event now-on" style="background-image: url('<?php echo get_the_post_thumbnail_url(66); ?>');">
                <div class="calendar-block__event--details">
                    <a href="<?php echo esc_url( get_page_link( 66 ) ); ?>" ><h3 class="calendar-block__event--title"><?php echo get_the_title(66); ?></h3></a>
                    <a class="btn" href="<?php echo esc_url( get_page_link( 66 ) ); ?>">FIND OUT MORE</a>
                </div>
            </div>
            <div class="calendar-block__event coming-soon" style="background-image: url('<?php echo get_the_post_thumbnail_url(42); ?>');">
                <div class="calendar-block__event--details">
                    <a href="<?php echo esc_url( get_page_link( 42 ) ); ?>" ><h3 class="calendar-block__event--title"><?php echo get_the_title(42); ?></h3></a>
                    <a class="btn" href="<?php echo esc_url( get_page_link( 42 ) ); ?>">FIND OUT MORE</a>
                </div>
            </div>
            <div class="calendar-block__event coming-soon" style="background-image: url('<?php echo get_the_post_thumbnail_url(56); ?>');">
                <div class="calendar-block__event--details">
                    <a href="<?php echo esc_url( get_page_link( 56 ) ); ?>" ><h3 class="calendar-block__event--title"><?php echo get_the_title(56); ?></h3></a>
                    <a class="btn" href="<?php echo esc_url( get_page_link( 56 ) ); ?>">FIND OUT MORE</a>
                </div>
            </div>
            <div class="calendar-block__event coming-soon" style="background: url('<?php echo get_the_post_thumbnail_url(69); ?>');">
                <div class="calendar-block__event--details">
                    <a href="<?php echo esc_url( get_page_link( 69 ) ); ?>" ><h3 class="calendar-block__event--title"><?php echo get_the_title(69); ?></h3></a>
                    <a class="btn" href="<?php echo esc_url( get_page_link( 69 ) ); ?>">FIND OUT MORE</a>
                </div>
            </div>
            <div class="calendar-block__event always-on" style="background-image: url('<?php echo get_the_post_thumbnail_url(63) ?>');">
                <div class="calendar-block__event--details">
                    <a href="/tickets/" ><h3 class="calendar-block__event--title"><?php echo get_the_title(63); ?></h3></a>
                    <a class="btn" href="/tickets/">FIND OUT MORE</a>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="instagram-blockhp">
    <img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/11/instagram_PNG12.png" alt="" class="logo_insta">
    <?php echo do_shortcode('[instagram-feed  followtext="Follow"]'); ?>
</div>
<?php get_footer(); ?>

